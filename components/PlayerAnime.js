import React from 'react';
import { StyleSheet, Animated, Text, ImageBackground, View } from 'react-native';
import primaryTheme from '../styles/Theme';

export default class Card extends React.Component{
    constructor(props){
        super(props);
        this.rollImg = Math.floor(Math.random() * 3); // what logo img  
        this.whatImg = [primaryTheme.$player1,primaryTheme.$player2,primaryTheme.$player3];
        this.state = {
            isVisible: this.props.isVisible,
            rotate: this.props.rotate,
            leftPosition: this.props.left,
            rightPosition: this.props.right,
            topPosition: this.props.top,
            bottomPosition: this.props.bottom,
            player: this.props.player,
        }
    }

    render(){
        // console.debug(this.state.player)
        return (
            this.state.isVisible ? 
                <ImageBackground source={this.whatImg[this.rollImg]} style={[styles.player,{left: this.state.leftPosition, right: this.state.rightPosition, top: this.state.topPosition, bottom: this.state.bottomPosition, transform:[{rotate: this.state.rotate}]}]}>
                    <View style={{width:60,height:40,backgroundColor:'rgba(255,255,255,0.5)',top:-20,flexDirection:'column',alignItems:'center'}}>
                        <Text style={{fontWeight:'bold',top:-20,color:'white'}}>{
                            this.state.player.getPosition()
                        }</Text>
                        <Text style={{fontWeight:'bold',top:-20}}>{this.state.player.getCards().cardOne[0]},{this.state.player.getCards().cardTwo[0]}</Text>
                        <Text style={{fontWeight:'bold',top:-20}}>${this.state.player.getCoins()}</Text>
                    </View>
                </ImageBackground> 
            : null
        );
    }
}

const styles = StyleSheet.create({
    player: {width:80,height:80,position:'absolute',resizeMode:'stretch',flexDirection:'column',alignItems:'center',
  },
});
