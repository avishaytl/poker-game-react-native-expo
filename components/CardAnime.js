import React from 'react';
import { StyleSheet, Animated, Text, Image, View } from 'react-native';
import primaryTheme from '../styles/Theme';

export default class Card extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            value: this.props.value,
            suits: this.props.suits,
            leftPosition: this.props.left,
            rightPosition: this.props.right,
            topPosition: this.props.top,
            bottomPosition: this.props.bottom,
        } 
    }
    render(){
        let style;
        this.props.style === undefined ? style = styles.card : style = this.props.style;
        return (
        <Animated.View style={[style,{left: this.state.leftPosition, right: this.state.rightPosition, top: this.state.topPosition, bottom: this.state.bottomPosition}]}>
            <Text style={{color:'black',fontSize:30,fontWeight:'bold'}}>{this.state.value}</Text>
            <Image style={{width:30,height:25,resizeMode:'stretch'}} source={this.state.suits}/>
        </Animated.View>
        );
    }
}

const styles = StyleSheet.create({
  card: {
    width:100,
    height:140,
    backgroundColor:'white',
    borderRadius:8,
    borderColor:'black',
    borderWidth:2,
    position:'absolute',
    flexDirection:'column',
    alignItems:'center',
    justifyContent:'center',
  },
});
