import { Dimensions, StatusBar } from 'react-native';
import { getStatusBarHeight } from 'react-native-status-bar-height';// ** device width&height **
StatusBar.setHidden(true);
const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
const statusbar = getStatusBarHeight();
console.log(width + ' width');
console.log(height + ' height');
console.log(statusbar + ' statusBar');
const primaryTheme = {
    // ** device info **
        $deviceWidth: width,
        $deviceHeight: height,
        $deviceStatusBar: statusbar,
    //game info
        $backgroundGame: require('./images/table.png'),
        $card: require('./images/card.png'),
        $backCard: require('./images/backCard.png'),
        $spades: require('./images/spades.png'),
        $clubs: require('./images/clubs.png'),
        $diamonds: require('./images/diamonds.png'),
        $hearts: require('./images/hearts.png'),
        $player1: require('./images/player1.png'),
        $player2: require('./images/player2.png'),
        $player3: require('./images/player3.png'),
        $coin5: require('./images/coin5.png'),
        $coin10: require('./images/coin10.png'),
        $coin20: require('./images/coin20.png'),
        $coin50: require('./images/coin50.png'),
        $coin100: require('./images/coin100.png'),
        $coin500: require('./images/coin500.png'),
        $coin1k: require('./images/coin1k.png'),
        $coin5k: require('./images/coin5k.png'),
        $tableCoins: require('./images/rightCoinsTable.png'),
}
export default primaryTheme;