import React from 'react';
import { StyleSheet, Animated, View, Text, FlatList, Image, ImageBackground, TouchableOpacity } from 'react-native';
import { ScreenOrientation } from 'expo';
import { LinearGradient } from 'expo-linear-gradient';
import Table from './classes/Table';
import CardAnime from './components/CardAnime';
import PlayerAnime from './components/PlayerAnime';
import primaryTheme from './styles/Theme';
import AwesomeButton from 'react-native-really-awesome-button';
import { Icon } from 'native-base';
const DATA = [
  {
    id: '0',
    value: 5,
    btnSource: primaryTheme.$coin5,
    btnBorderRadius: 35,
    btnColor: '#ab0159',
  },
  {
    id: '1',
    value: 10,
    btnSource: primaryTheme.$coin10,
    btnBorderRadius: 35,
    btnColor: '#354580',
  },
  {
    id: '2',
    value: 20,
    btnSource: primaryTheme.$coin20,
    btnBorderRadius: 35,
    btnColor: '#6b0006',
  },
  {
    id: '3',
    value: 50,
    btnSource: primaryTheme.$coin50,
    btnBorderRadius: 35,
    btnColor: '#1483ae',
  },
  {
    id: '4',
    value: 100,
    btnSource: primaryTheme.$coin100,
    btnBorderRadius: 35,
    btnColor: '#a20014',
  },
  {
    id: '5',
    value: 500,
    btnSource: primaryTheme.$coin500,
    btnBorderRadius: 35,
    btnColor: '#557840',
  },
  {
    id: '6',
    value: 1000,
    btnSource: primaryTheme.$coin1k,
    btnBorderRadius: 35,
    btnColor: '#bc7f00',
  },
  {
    id: '7',
    value: 5000,
    btnSource: primaryTheme.$coin5k,
    btnBorderRadius: 35,
    btnColor: '#c58d32',
  },
];
const playersNumber = 7; // max = 7, min 3, just try ;)
export default class App extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      table: new Table(playersNumber),
      gamePlayCards: '',
      players: [],
      delaerPosition: null,
      playersView:[
        playerVisible1 = false,
        playerVisible2 = false,
        playerVisible3 = false,
        playerVisible4 = false,
        playerVisible5 = false,
        playerVisible6 = false,
      ],
      player:{
        id: '',
        coins: 0,
        bet: 0,
        playerSuitsOne: '',
        playerSuitsTwo: '',
        playerValueOne: '',
        playerValueTwo: '',
      },
      startScreenAnime: new Animated.Value(1),
      cardOneAnime: new Animated.Value(-150),
      cardTwoAnime: new Animated.Value(-150),
      gameCardOneBurenAnime: new Animated.Value(-350),
      gameCardTwoBurenAnime: new Animated.Value(-350),
      gameCardThreeBurenAnime: new Animated.Value(-350),
      gameCardOneAnime: new Animated.Value(-350),
      gameCardTwoAnime: new Animated.Value(-350),
      gameCardThreeAnime: new Animated.Value(-350),
      gameCardFourAnime: new Animated.Value(-350),
      gameCardFiveAnime: new Animated.Value(-350),
      finishedAnime: false,
      $Bet_One: 'Bet_One',
      $Bet_Two: 'Bet_Two',
      $Bet_Three: 'Bet_Three',
      $End_Game: 'End_Game',
    }
    for(i = 0;i < (playersNumber - 1) || i < 2;i++){
      this.state.player['playerVisible' + (i + 1)] = true;
      // console.debug('Player' + (i+1) + ' Visible: ' + this.state.player['playerVisible' + (i + 1)]);
    }
    this.startPlayGame();
    console.debug('~~~~~~~~~~~~~~~~~~ GAME FLOP ~~~~~~~~~~~~~~~~~');
    console.debug(this.state.gamePlayCards);
    this.setGamePlayFlopCards(this.state.gamePlayCards);
    console.debug('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~');
  }
  componentDidMount() {
    ScreenOrientation.lockAsync(ScreenOrientation.Orientation.LANDSCAPE_RIGHT);
  }

  componentWillMount() {
    ScreenOrientation.lockAsync(ScreenOrientation.Orientation.PORTRAIT);
  }
  startPlayGame = () => {
    this.state.players = this.state.table.playGame();
    this.state.gamePlayCards = this.state.table.getGamePlayCards();
    this.state.players = this.state.table.getPlayers();
    this.state.delaerPosition = this.state.table.getDealerPosition(); 
    this.state.player.playerSuitsOne = this.setSuitsInCardOne(this.state.players);
    this.state.player.playerSuitsTwo = this.setSuitsInCardTwo(this.state.players);
    this.state.player.playerValueOne = this.state.players[this.state.delaerPosition].getCards().cardOne[0];
    this.state.player.playerValueTwo = this.state.players[this.state.delaerPosition].getCards().cardTwo[0];
    // this.state.players.bet = 0;
  }
  playAgian = () => {

  }
  onStart = () => {
    Animated.timing(this.state.startScreenAnime,{
      toValue: 0,
      duration: 700,
      // boomerang: true,
    }).start(this.setState({finishedAnime: true}));
    Animated.timing(this.state.cardOneAnime,{
      toValue: primaryTheme.$deviceWidth - 120,
      duration: 700,
    }).start();
    Animated.timing(this.state.cardTwoAnime,{
      toValue: primaryTheme.$deviceWidth - 120,
      duration: 700,
    }).start();
    setTimeout(() => {
      Animated.timing(this.state.gameCardOneBurenAnime,{
        toValue: -20,
        duration: 700,
      }).start();
    }, 1000);
    setTimeout(() => {
      Animated.timing(this.state.gameCardTwoBurenAnime,{
        toValue: -10,
        duration: 700,
      }).start();
    }, 1100);
    setTimeout(() => {
      Animated.timing(this.state.gameCardThreeBurenAnime,{
        toValue: 0,
        duration: 700,
      }).start();
    }, 1200);
    setTimeout(() => {
      Animated.timing(this.state.gameCardOneAnime,{
        toValue: 110,
        duration: 700,
      }).start();
    }, 1300);
    setTimeout(() => {
      Animated.timing(this.state.gameCardTwoAnime,{
        toValue: 110,
        duration: 700,
      }).start();
    }, 1400);
    setTimeout(() => {
      Animated.timing(this.state.gameCardThreeAnime,{
        toValue: 110,
        duration: 700,
      }).start();
    }, 1500);
    setTimeout(() => {
      Animated.timing(this.state.gameCardFourAnime,{
        toValue: 110,
        duration: 700,
      }).start();
    }, 1600);
    setTimeout(() => {
      Animated.timing(this.state.gameCardFiveAnime,{
        toValue: 110,
        duration: 700,
      }).start();
    }, 1700);
    this.setState({player:{coins: this.state.players[this.state.delaerPosition].getCoins(), bet: 0}});
    console.debug('delaer Position: ' + this.state.delaerPosition);
    console.debug('animeDone: ' + this.state.finishedAnime);
  }
  setGamePlayFlopCards = (gamePlayCards) => {
    let arraySuitsCards =[];
    arraySuitsCards = gamePlayCards;
    Object.keys(arraySuitsCards).forEach((key,val)=>{
      switch(arraySuitsCards[key][1]){
        case 'Spades':
            arraySuitsCards[key][1] = primaryTheme.$spades;
          break;
        case 'Diamonds':
            arraySuitsCards[key][1] = primaryTheme.$diamonds;
          break;
        case 'Hearts':
            arraySuitsCards[key][1] = primaryTheme.$hearts;
          break;
        case 'Clubs':
            arraySuitsCards[key][1] = primaryTheme.$clubs;
          break;
      }
    });
  }
  setSuitsInCardOne = (players) => {
    console.debug( 'card1 => ' + players[this.state.delaerPosition].getCards().cardOne[0]); // card one
    console.debug( 'suits1 => ' + players[this.state.delaerPosition].getCards().cardOne[1]); 
    switch(players[this.state.delaerPosition].getCards().cardOne[1]){
      case 'Spades':
        return primaryTheme.$spades;
      case 'Diamonds':
        return primaryTheme.$diamonds;
      case 'Hearts':
        return primaryTheme.$hearts;
      case 'Clubs':
        return primaryTheme.$clubs;
    }
  }
  setSuitsInCardTwo = (players) => {
    console.debug( 'card2 => ' + players[this.state.delaerPosition].getCards().cardTwo[0]); // card two
    console.debug( 'suits2 => ' + players[this.state.delaerPosition].getCards().cardTwo[1]);
    switch(players[this.state.delaerPosition].getCards().cardTwo[1]){
      case 'Spades':
        return primaryTheme.$spades;
      case 'Diamonds':
        return primaryTheme.$diamonds;
      case 'Hearts':
        return primaryTheme.$hearts;
      case 'Clubs':
        return primaryTheme.$clubs;
    }
  }
  getItemLayout = (data, index) => (
    { length: 70, offset: 70 * index, index }
  )
  render(){
    Item = ({btnSource,btnRadius,btnColor,addBet}) =>{
      return (
        <AwesomeButton height={70}width={70} borderRadius={btnRadius}
          onPress={()=>{this.setState({player:{coins: this.state.players[this.state.delaerPosition].getCoins(), bet: (this.state.player.bet + addBet)}})}}
          backgroundDarker={btnColor}
          backgroundColor={btnColor}
          style={{height:70,width:70,borderRadius:btnRadius,flexDirection:'column',justifyContent:'center',alignItems:'center',marginHorizontal:5}}>
          <Image style={{height:70,width:70}} source={btnSource}/>
        </AwesomeButton>
      );
    }
    return (
      <ImageBackground style={styles.container} source={primaryTheme.$backgroundGame}>
        {/* player1 Anime */}
        <PlayerAnime player={this.state.players[0]} isVisible={this.state.player['playerVisible1']} top={40} right={150} rotate={'180deg'}/>
        {/* player2 Anime */}
        <PlayerAnime player={this.state.players[1]} isVisible={this.state.player['playerVisible2']} top={180} right={70} rotate={'270deg'}/>
        {/* player3 Anime */}
        <PlayerAnime player={this.state.players[2]} isVisible={this.state.player['playerVisible3']} top={330} right={150} rotate={'0deg'}/>
        {/* player4 Anime */}
        <PlayerAnime player={this.state.players[3]} isVisible={this.state.player['playerVisible4']} top={330} left={150} rotate={'0deg'}/>
        {/* player5 Anime */}
        <PlayerAnime player={this.state.players[4]} isVisible={this.state.player['playerVisible5']} top={180} left={70} rotate={'90deg'}/>
        {/* player6 Anime */}
        <PlayerAnime player={this.state.players[5]} isVisible={this.state.player['playerVisible6']} top={40} left={150} rotate={'180deg'}/>
        <View style={styles.gameTableView}>
          <Animated.Image style={[styles.gameCard,{left:100,top: this.state.gameCardOneBurenAnime}]} source={primaryTheme.$card}/>
          <Animated.Image style={[styles.gameCard,{left:60,top: this.state.gameCardTwoBurenAnime}]} source={primaryTheme.$card}/>
          <Animated.Image style={[styles.gameCard,{left:20,top: this.state.gameCardThreeBurenAnime}]} source={primaryTheme.$card}/>
          <CardAnime style={styles.card} left={200} top={this.state.gameCardOneAnime} value={this.state.gamePlayCards['gameCardOne'][0]} suits={this.state.gamePlayCards['gameCardOne'][1]}/>
          <CardAnime style={styles.card} left={250} top={this.state.gameCardTwoAnime} value={this.state.gamePlayCards['gameCardTwo'][0]} suits={this.state.gamePlayCards['gameCardTwo'][1]}/>
          <CardAnime style={styles.card} left={300} top={this.state.gameCardThreeAnime} value={this.state.gamePlayCards['gameCardThree'][0]} suits={this.state.gamePlayCards['gameCardThree'][1]}/>
          <CardAnime style={styles.card} left={350} top={this.state.gameCardFourAnime} value={this.state.gamePlayCards['gameCardFour'][0]} suits={this.state.gamePlayCards['gameCardFour'][1]}/>
          <CardAnime style={styles.card} left={400} top={this.state.gameCardFiveAnime} value={this.state.gamePlayCards['gameCardFive'][0]} suits={this.state.gamePlayCards['gameCardFive'][1]}/>
        </View>
        {/* PLAYER */}
        <View style={styles.playerCards}>
          <CardAnime left={10} top={this.state.cardOneAnime} value={this.state.player.playerValueOne} suits={this.state.player.playerSuitsOne}/>
          <CardAnime right={10} top={this.state.cardTwoAnime} value={this.state.player.playerValueTwo} suits={this.state.player.playerSuitsTwo}/>
        </View>
        <Animated.View style={[styles.firstScreen,{opacity: this.state.startScreenAnime}]}>
            <AwesomeButton onPress={this.onStart.bind(this)}
              ExtraContent={
                <LinearGradient colors={['rgb(255,204,94)', 'rgb(255,233,129)', 'rgb(111,58,0)','rgb(255,204,94)']} style={{width:'100%',height:'100%',padding:2}}>
                  <View style={{borderRadius: 50,width:'100%',height:'100%', display:'flex', flexDirection:'row',alignItems:'center',justifyContent:'center',backgroundColor:'black'}}>
                    <Icon style={{margin:5,color: 'white',opacity:0.8,fontSize:25}} name={'logo-game-controller-a'}/>
                      <Text style={[styles.buttonText,{opacity:0.8,fontSize: 20}]}>START</Text>
                  </View>
                </LinearGradient>}
              borderRadius={50}
              width={180}
              height={44}
              backgroundDarker={'#c58d32'}
              style={{margin:5}}>
            <Text></Text>
          </AwesomeButton>
        </Animated.View>
        <View style={{top:0,left:10,position:'absolute',opacity:0.5,alignItems:'flex-start'}}>
            <Text style={{fontSize:50,color:'white',fontWeight:'bold'}}>${this.state.player.coins}</Text>
        </View>
        <View style={{top:260,right:50,position:'absolute',opacity:0.5,alignItems:'flex-end'}}>
            <Text style={{fontSize:30,color:'white',fontWeight:'bold'}}>{this.state.player.bet} ~ BET</Text>
        </View>
        <TouchableOpacity  onPress={()=>{this.setState({player:{coins: this.state.players[this.state.delaerPosition].getCoins(), bet: (0)}})}}
              style={{right:10,top:265,width:35,height:35,borderRadius:20,borderColor:'red',borderWidth:3,backgroundColor:'black',position:'absolute'}}>
             <Icon style={{color: 'white'}} name={'logo-game-controller-a'}/>
        </TouchableOpacity>
        <TouchableOpacity  onPress={
          ()=>{
            this.setState({
              table: new Table(playersNumber),
              player:{
                id: '',
                coins: 0,
                bet: 0,
                playerSuitsOne: '',
                playerSuitsTwo: '',
                playerValueOne: '',
                playerValueTwo: '',
              }});
              this.state.cardOneAnime.setValue(-150);
              this.state.cardTwoAnime.setValue(-150);
              this.state.gameCardOneBurenAnime.setValue(-350);
              this.state.gameCardTwoBurenAnime.setValue(-350);
              this.state.gameCardThreeBurenAnime.setValue(-350);
              this.state.gameCardOneAnime.setValue(-350);
              this.state.gameCardTwoAnime.setValue(-350);
              this.state.gameCardThreeAnime.setValue(-350);
              this.state.gameCardFourAnime.setValue(-350);
              this.state.gameCardFiveAnime.setValue(-350);
            this.startPlayGame();
            this.setGamePlayFlopCards(this.state.gamePlayCards);
            this.onStart();
          }
        }
              style={{left:10,top:265,width:35,height:35,borderRadius:20,borderColor:'red',borderWidth:3,backgroundColor:'black',position:'absolute'}}>
             <Icon style={{color: 'white'}} name={'ios-arrow-dropup-circle'}/>
        </TouchableOpacity>
        {/* PLAYER VIEW */}
        <Animated.View style={{zIndex:2,right:10,bottom:10,width:250,height:100,flexDirection:'column',alignItems:'center',justifyContent:'center',position:'absolute',resizeMode:'stretch'}}>
          <ImageBackground source={primaryTheme.$tableCoins} style={{overflow:'hidden',borderWidth:4,borderColor:'black',borderRadius:50,width:250,height:100,flexDirection:'column',justifyContent:'center',alignItems:'center'}} resizeMode='stretch'>
          <FlatList
            width={250}
            height={70}
            horizontal={true}
            data={DATA}
            ref={ref => (this.flatList = ref)}
            keyExtractor={item => item.id}
            getItemLayout={(data, index) => this.getItemLayout(data, index)}
            renderItem={({ item }) => <Item id={item.id} btnSource={item.btnSource} btnRadius={item.btnBorderRadius} btnColor={item.btnColor} addBet={item.value}/>}
            style={{position:'absolute'}}
            initialScrollIndex={3}
            scrollEnabled={true}
            />
          </ImageBackground>
        </Animated.View>
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  container:{
    width: primaryTheme.$deviceHeight,
    height: primaryTheme.$deviceWidth,
    flexDirection:'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  firstScreen:{
    position:'absolute',
    zIndex:1000,
    backgroundColor:'rgba(0,0,0,0.6)',
    width: primaryTheme.$deviceHeight,
    height: primaryTheme.$deviceWidth,
    flexDirection:'column',
    alignItems: 'center',
    justifyContent: 'center',
  },
  gameTableView:{
    width:600,height:300,position:'absolute',flexDirection:'row',alignItems:'center',justifyContent:'flex-start'
  },
  card:{
    width:60,
    height:85,
    backgroundColor:'white',
    borderRadius:8,
    borderColor:'black',
    borderWidth:2,
    position:'absolute',
    flexDirection:'column',
    alignItems:'center',
    justifyContent:'center',
  },
  gameCard:{
    width:60,
    height:85,
    resizeMode:'stretch',
    left:100,
  },
  playerCards:{width:250,height:primaryTheme.$deviceWidth,flexDirection:'column',alignItems:'center',justifyContent:'center'
  },
  button: {
    display:'flex', 
    flexDirection:'column',
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%',
    height: '100%',
    padding: 4,
    borderRadius: 25,
  },
  buttonText:{
    color: 'white',
    fontWeight: 'bold',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
