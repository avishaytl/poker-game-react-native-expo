import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Card from './Card'

export default class CardsPack {
    constructor(){
        this.state = {
            suits: ['Clubs','Diamonds','Hearts','Spades'],
            cards: [],
            index: 0,
        }
        this.setCardsInPack();
    }
    setCardsInPack = () => { 
        for(j = 0;j < 4;j++)
            for(i = 0;i < 13;i++){
                switch(j){
                    case 0:
                            this.state.cards.push( new Card( this.state.suits[j], (i + 1 == 11 ? 'J' : i + 1 == 12 ?  'Q' : i + 1 == 13 ?  'K' : i + 1 == 1 ?  'A' : i + 1) ));
                        break;
                    case 1:
                            this.state.cards.push( new Card( this.state.suits[j], (i + 1 == 11 ? 'J' : i + 1 == 12 ?  'Q' : i + 1 == 13 ?  'K' : i + 1 == 1 ?  'A' : i + 1) ));
                        break;
                    case 2:
                            this.state.cards.push( new Card( this.state.suits[j], (i + 1 == 11 ? 'J' : i + 1 == 12 ?  'Q' : i + 1 == 13 ?  'K' : i + 1 == 1 ?  'A' : i + 1) ));
                        break;
                    case 3:
                            this.state.cards.push( new Card( this.state.suits[j], (i + 1 == 11 ? 'J' : i + 1 == 12 ?  'Q' : i + 1 == 13 ?  'K' : i + 1 == 1 ?  'A' : i + 1) ));
                        break;
                }
            }
        let shuffle = require('shuffle-array');
        let result = [];
        this.state.cards.forEach((val)=>{
            result.push([val.getValue(),val.getSuits()]);
        })
        this.state.cards = shuffle(result);
        this.state.cards = shuffle(this.state.cards);
    }
    getCardsInPack = () => {
        return this.state.cards;
    }
    getIndex = () => {
        return this.state.index;
    }
    setIndex = (index) => {
        this.state.index = index;
    }
    burenNextCardInPack = () => {
        this.state.index++;
    }
    getNextCardInPack = () => {
        return this.state.cards[this.state.index++];
    }
    render(){
        return (
        <View style={styles.container}>
            <Text>Open up App.js to start working on your app!</Text>
        </View>
        );
    }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
