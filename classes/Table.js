import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Player from './Player';
import CardsPack from './CardsPack';
export default class Table {
  constructor(playerCount = 3){
      playerStart = Math.floor(Math.random() * Math.floor(playerCount));
      playerCount < 3 ? playerCount = 3 : playerCount > 7 ? playerCount = 7 : null;
      // playerStart = 4;
      this.state = {
          players: [],
          playersCount: playerCount,
          players: [],
          gameCards:{
            gameCardOne: '',
            gameCardTwo: '',
            gameCardThree: '',
            gameCardFour: '',
            gameCardFive: '',
          },
          cardsPack: new CardsPack(),
          gameRoundSum: 0,
          delaerPlayer: playerStart,
          smallPlayer: playerStart == playerCount - 1 ? 0 : playerStart == playerCount - 2 ? playerStart + 1 : playerStart + 1,
          bigPlayer: playerStart == playerCount - 1 ? 1 : playerStart == playerCount - 2 ? 0 : playerStart + 2,
      }
      this.setPlayers(this.state.playersCount);
      for(i = 0;i < this.state.playersCount;i++){
        console.debug( 'playerId => ' + this.state.players[i].getId() );
        console.debug( 'position: ' + this.state.players[i].getPosition() );
        console.debug( 'cardOne: ' + this.state.players[i].getCards().cardOne );
        console.debug( 'cardTwo: ' + this.state.players[i].getCards().cardTwo );
      }
  }
  setPlayers = (count) => {
    console.debug('SetPlayers_function: Players In Game ~ ' + count);
    for(i = 0;i < count;i++){ // build 3 players in game
      this.state.players.push( new Player('Player_' + (i + 1)) )
    }
    this.state.players[this.state.delaerPlayer].setPosition('delaer');
    this.state.players[this.state.smallPlayer].setCards(this.state.cardsPack.getNextCardInPack(),this.state.cardsPack.getNextCardInPack());
    this.state.players[this.state.smallPlayer].setPosition('small');
    this.state.players[this.state.bigPlayer].setCards(this.state.cardsPack.getNextCardInPack(),this.state.cardsPack.getNextCardInPack());
    this.state.players[this.state.bigPlayer].setPosition('big');
    index = this.state.bigPlayer + 1;
    for(i = 0; i <  count - 3;i++){
      if(index == this.state.playersCount)
        index = 0;
      this.state.players[ index ].setCards(this.state.cardsPack.getNextCardInPack(),this.state.cardsPack.getNextCardInPack());
      this.state.players[ index ].setPosition('regular');
      index++
    }
    //set Delaer cards
    this.state.players[this.state.delaerPlayer].setCards(this.state.cardsPack.getNextCardInPack(), this.state.cardsPack.getNextCardInPack());
  }
  getPlayers = () => {
    return this.state.players;
  }
  getDealerPosition = () => {
    return this.state.delaerPlayer;
  }
  playGame = () => {
    //buren start game card
    this.state.cardsPack.burenNextCardInPack();
    this.state.gameCards.gameCardOne = this.state.cardsPack.getNextCardInPack();
    this.state.gameCards.gameCardTwo = this.state.cardsPack.getNextCardInPack();
    this.state.gameCards.gameCardThree = this.state.cardsPack.getNextCardInPack();
    //buren second game card
    this.state.cardsPack.burenNextCardInPack();
    this.state.gameCards.gameCardFour = this.state.cardsPack.getNextCardInPack();
    //buren third game card
    this.state.cardsPack.burenNextCardInPack();
    this.state.gameCards.gameCardFive = this.state.cardsPack.getNextCardInPack();
  }
  getGamePlayCards = () => {
    return this.state.gameCards;
  }
  setBetRound = () => {

  }
  render(){
    return (
      <View style={styles.container}>
        <Text>Open up App.js to start working on your app!</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
