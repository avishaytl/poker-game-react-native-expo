import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

export default class Player {
    constructor(id = '', cardOne = '', cardTwo = '', pos = '', coins = 1500){
        this.state = {
            id: id,
            cards: {
                cardOne: cardOne, cardTwo: cardTwo,
            },
            position: pos,
            coins: coins,
        }
    }
    getId = () => {
        return this.state.id;
    }
    setId = (id) => {
        this.state.id = id;
    }
    getCards = () => {
        return this.state.cards;
    }
    setCards = (cardOne, cardTwo) => {
        this.state.cards.cardOne = cardOne;
        this.state.cards.cardTwo = cardTwo;
    }
    getPosition = () => {
        return this.state.position;
    }
    setPosition = (pos) => {
        this.state.position = pos;
    }
    getCoins = () => {
        return this.state.coins;
    }
    setCoins = (coins) => {
        return this.state.coins += coins;
    }
    incCoins = (coins) => {
        this.state.coins += coins;
    }
    decCoins = (coins) => {
        this.state.coins -= coins;
    }
    render(){
        return (
        <View style={styles.container}>
            <Text>Open up App.js to start working on your app!</Text>
        </View>
        );
    }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
