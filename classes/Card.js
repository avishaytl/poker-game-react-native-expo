import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

export default class Card {
    constructor(suits  = 'suits', value = 'color'){
        this.state = {
            value: value,
            suits: suits,
        }
    }
    getValue = () => {
        return this.state.value;
    }
    setValue = (val) => {
        this.state.value = val;
    }
    getSuits = () => {
        return this.state.suits ;
    }
    setSuits = (suits) => {
        this.state.suits = suits;
    }
    render(){
        return (
        <View style={styles.container}>
            <Text>Open up App.js to start working on your app!</Text>
        </View>
        );
    }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
